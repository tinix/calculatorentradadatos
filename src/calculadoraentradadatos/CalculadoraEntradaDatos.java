package calculadoraentradadatos;

import java.util.*;

import static calculadora.Operaciones.sumar;

/**
 *
 * @author tinix
 */
public class CalculadoraEntradaDatos {

    public static void main(String[] args) {
        System.out.println("Proposciona el primer valor");
        Scanner scaner = new Scanner(System.in);
        int a = scaner.nextInt();
        System.out.println("Proporciona el segundo valor");
        int b = scaner.nextInt();
        int result = sumar(a, b);
        
        System.out.println("El resultado del la suma es : " + result);
    }
    
}
